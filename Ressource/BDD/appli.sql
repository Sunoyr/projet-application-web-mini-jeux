-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 13 déc. 2018 à 14:16
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `appli`
--

-- --------------------------------------------------------

--
-- Structure de la table `motsmeles`
--

DROP TABLE IF EXISTS `motsmeles`;
CREATE TABLE IF NOT EXISTS `motsmeles` (
  `userID` int(11) NOT NULL,
  `highscore` int(11) NOT NULL,
  `resolveCounter` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `motus`
--

DROP TABLE IF EXISTS `motus`;
CREATE TABLE IF NOT EXISTS `motus` (
  `userID` int(11) NOT NULL,
  `highscore` int(11) NOT NULL,
  `resolveCounter` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pendu`
--

DROP TABLE IF EXISTS `pendu`;
CREATE TABLE IF NOT EXISTS `pendu` (
  `userID` int(11) NOT NULL,
  `highscore` int(11) NOT NULL,
  `resolveCounter` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `sudoku`
--

DROP TABLE IF EXISTS `sudoku`;
CREATE TABLE IF NOT EXISTS `sudoku` (
  `userID` int(11) NOT NULL,
  `highscore` int(11) NOT NULL,
  `resolveCounter` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `sudokulettres`
--

DROP TABLE IF EXISTS `sudokulettres`;
CREATE TABLE IF NOT EXISTS `sudokulettres` (
  `userID` int(11) NOT NULL,
  `highscore` int(11) NOT NULL,
  `resolveCounter` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `admin` bit(1) NOT NULL DEFAULT b'0',
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `pseudo` varchar(30) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `sexe` varchar(1) NOT NULL,
  `password` varchar(100) NOT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `motsmeles`
--
ALTER TABLE `motsmeles`
  ADD CONSTRAINT `FK_ID` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `motus`
--
ALTER TABLE `motus`
  ADD CONSTRAINT `FK_ID2` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Contraintes pour la table `pendu`
--
ALTER TABLE `pendu`
  ADD CONSTRAINT `FK_ID3` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sudoku`
--
ALTER TABLE `sudoku`
  ADD CONSTRAINT `FK_ID4` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Contraintes pour la table `sudokulettres`
--
ALTER TABLE `sudokulettres`
  ADD CONSTRAINT `FK_ID5` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
