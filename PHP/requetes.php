<?php
// ceci est un fichier de requêtes principalement SQL qui est appelé par d'autres pages pour effectuer
//des actions côté serveur
session_start();
include 'tools.php';

    if (isset($_POST['functionname'])) {
        switch ($_POST['functionname']) {
            //cas ou l'on veut ajouter un score
            case 'add':
                addPoints($bdd, $_POST['arguments'][2], $_POST['arguments'][1]);
                highscoreJeux($bdd, $_POST['arguments'][2], $_POST['arguments'][0], $_POST['arguments'][1]);
                break;
                // cas où le leaderboard demande un changement d'ordre
            case 'leader':
                $cpt = 0;
                $retour = "";
                if ($_POST['arguments'][0]=='general'){

                    $sql = "SELECT * FROM users ORDER BY score DESC";
                    $stmt = $bdd -> prepare($sql);
                    $stmt -> execute();
                    while ($res = $stmt -> fetch()){
                        $cpt++;
                        $retour = $retour . "<tr> <td>" . $cpt ."</td> <td>" . $res['pseudo'] ." </td> <td> " . $res['score'] ."</td></tr>";

                    }
                    echo $retour;

                }
                else{

                    $jeuLeader = $_POST['arguments'][0];
                    $sql = "SELECT users.pseudo, $jeuLeader.userID, $jeuLeader.highscore FROM $jeuLeader INNER JOIN users ON $jeuLeader.userID = users.userID ORDER BY $jeuLeader.highscore DESC";
                    $stmt = $bdd -> prepare($sql);
                    $stmt -> execute();
                    while ($res = $stmt -> fetch()){


                        $cpt++;
                        $retour = $retour . "<tr> <td>" . $cpt ."</td> <td>" . $res[0] ." </td> <td> " . $res[2] ."</td></tr>";

                    }
                    echo $retour;

                }
                break;
                // cas où l'on demande une vérification de mot
            case 'verifDico':

                if (strpos(file_get_contents("../Ressource/Dictionnaire/dico.txt"), $_POST['arguments']) !== false) echo '1';
                /*$handle = fopen("../Ressource/Dictionnaire/dico.txt", "r");
                if ($handle) {

                    while ((($line = fgets($handle)) !== false)) {
                        echo $line==$_POST['arguments'];
                        if ($line == $_POST['arguments']){
                            echo '1';
                        }

                    }

                }*/
                break;
            default:
                echo 'error';
        }
    }

    //fonction de connexion
    function connexion($bdd,$login,$password,$redirect){
        $sql="SELECT * FROM users WHERE pseudo=:login";
        $stmt = $bdd -> prepare($sql);
        $stmt -> execute(array('login' => $login));
        $respass = $stmt->fetch();

        $redirect2 = NULL;


        if($respass){
            if (password_verify($password,$respass['password'])){ //Si le mot de passe saisi correspond au hachage dans la BDD
                $_SESSION['login']=$respass['pseudo'];

                if($redirect != '') {
                    $redirect2 = $redirect;
                }
                else{
                    $redirect2 = '../index.php';
                }

                header('Location:' . $redirect2); //Redirection

            }
            else{
                header('Location:connexionError.php'); //Redirection index avec erreur
            }
        }

        else{
            header('Location:connexionError.php');
        }
    }




    //inscription d'un utilisateur en BDD
    function inscription($bdd,$nom,$prenom,$pseudo,$mail,$sexe,$password){
        $hash = password_hash($password,PASSWORD_DEFAULT);

        $sql = "INSERT INTO users (nom, prenom, pseudo,mail, sexe, password, score) VALUES (:nom, :prenom,:pseudo,:mail,:sexe,:pass, :score)";
        $stmt = $bdd ->prepare($sql);
        $stmt -> execute(array(

            'nom'=>$nom,
            'prenom'=>$prenom,
            'pseudo'=>$pseudo,
            'mail' => $mail,
            'sexe' => $sexe,
            'pass' => $hash,
            'score' => 0


        ));

        $lastid = $bdd->lastInsertId();

        $sql2 = "INSERT INTO motus (userID, highscore, resolveCounter) VALUES (:userid, :highscore, :resolved)";
        $stmt2 = $bdd ->prepare($sql2);
        $stmt2 -> execute(array(
            'userid' => $lastid,
            'highscore' => 0,
            'resolved' => 0
        ));

        $sql2 = "INSERT INTO sudoku (userID, highscore, resolveCounter) VALUES (:userid, :highscore, :resolved)";
        $stmt2 = $bdd ->prepare($sql2);
        $stmt2 -> execute(array(
            'userid' => $lastid,
            'highscore' => 0,
            'resolved' => 0
        ));

        $sql2 = "INSERT INTO motsmeles (userID, highscore, resolveCounter) VALUES (:userid, :highscore, :resolved)";
        $stmt2 = $bdd ->prepare($sql2);
        $stmt2 -> execute(array(
            'userid' => $lastid,
            'highscore' => 0,
            'resolved' => 0
        ));

        $sql2 = "INSERT INTO pendu (userID, highscore, resolveCounter) VALUES (:userid, :highscore, :resolved)";
        $stmt2 = $bdd ->prepare($sql2);
        $stmt2 -> execute(array(
            'userid' => $lastid,
            'highscore' => 0,
            'resolved' => 0
        ));

        $sql2 = "INSERT INTO sudokulettres (userID, highscore, resolveCounter) VALUES (:userid, :highscore, :resolved)";
        $stmt2 = $bdd ->prepare($sql2);
        $stmt2 -> execute(array(
            'userid' => $lastid,
            'highscore' => 0,
            'resolved' => 0
        ));

        $sql2 = "INSERT INTO sudoku (userID, highscore, resolveCounter) VALUES (:userid, :highscore, :resolved)";
        $stmt2 = $bdd ->prepare($sql2);
        $stmt2 -> execute(array(
            'userid' => $lastid,
            'highscore' => 0,
            'resolved' => 0
        ));
    }

    //récupère le score d'un joueur
    function getPoints ($bdd, $login){
        $sql = "SELECT * FROM users WHERE pseudo=:pseudo";
        $stmt = $bdd -> prepare($sql);
        $stmt -> execute(array(
            'pseudo' => $login
        ));

        $res = $stmt->fetch();
        $pts = $res['score'];
        return $pts;
    }

//ajoute des points au score d'un joueur
    function addPoints ($bdd, $login, $points){
        $oldPoints = getPoints($bdd, $login);
        $newPoints = $oldPoints + $points;
        $sql = "UPDATE users SET score =:score WHERE pseudo=:pseudo";
        $stmt = $bdd->prepare($sql);
        $stmt->execute(array(
            'score' => $newPoints,
            'pseudo' => $login
        ));
    }

    //recupere le highscore du jeu terminé
    function chercheHighscore ($bdd, $login, $jeu){
        $sql = "SELECT * FROM  $jeu INNER JOIN users ON $jeu.userID = users.userID WHERE users.pseudo=:login";
        $stmt = $bdd -> prepare($sql);
        $stmt -> execute(array(
            'login' => $login
        ));
        $res = $stmt->fetch();
        $highscore = $res['highscore'];
        return $highscore;
    }

    //recupere le nombre de jeux terminés avec succès
    function getResolved ($bdd, $login, $jeu){
        $sql ="SELECT * FROM $jeu INNER JOIN users ON $jeu.userID = users.userID WHERE users.pseudo=:login";
        $stmt = $bdd -> prepare ($sql);
        $stmt -> execute(array(
            'login' => $login
        ));
        $res = $stmt->fetch();
        $resolved = $res['resolveCounter'];
        return $resolved;
    }

    //modifie le highscore d'un joueur si nécessaire
    function highscoreJeux ($bdd, $login, $jeu, $points){
        if ($points > chercheHighscore($bdd, $login, $jeu)){
            $sql ="UPDATE $jeu INNER JOIN users ON $jeu.userID = users.userID SET highscore =:highscore, resolveCounter=:resolus  WHERE users.pseudo=:login";
            $stmt = $bdd -> prepare ($sql);
            $stmt -> execute(array(
                'login' => $login,
                'highscore' => $points,
                'resolus' => getResolved($bdd, $login, $jeu) + 1
            ));
        }
        else{
            $sql ="UPDATE $jeu INNER JOIN users ON $jeu.userID = users.userID SET resolveCounter=:resolus  WHERE users.pseudo=:login";
            $stmt = $bdd -> prepare ($sql);
            $stmt -> execute(array(
                'login' => $login,
                'resolus' => getResolved($bdd, $login, $jeu) + 1
            ));
        }
    }




