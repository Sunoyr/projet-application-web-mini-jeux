
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="../Styles/styleHeaderFooter.css"/>


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript">
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    </script>

</head>


<header><?php require "header.php"; ?></header>
<body>



<br><br><br><br>

<div class="container">
    <div id="loginbox" style="margin :auto ;margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Espace de connexion </div>

            </div>

            <div style="padding-top:30px" class="panel-body" >



                <form id="loginform"  action="traitementConnexion.php" class="form-horizontal" role="form" method="post">
                    <?php
                    echo '<input type="hidden" name="location" value="';
                    if(isset($_GET['location'])) {
                        echo htmlspecialchars($_GET['location']);
                    }
                    echo '" />';
                    ?>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-password" type="text" class="form-control" name="login" placeholder="Identifiant">
                    </div>

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="passwd" placeholder="Mot de passe">
                    </div>

                    <div style="margin-top:10px" class="form-group">
                        <!-- Button -->

                        <div style="margin-left: 35%" class="col-sm-12 controls">
                            <input id="btn-login"  type="submit" value="Valider" class="btn btn-success"> </input>


                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
</body>