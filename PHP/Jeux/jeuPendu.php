<?php
    include "../requetes.php";
    error_reporting(0);

    if (!isset($_SESSION['login'])){
        header('Location:../connexion.php?location=' . urlencode($_SERVER['REQUEST_URI']));
    }

    // stocke le pseudo de l'utilisateur
    $login = $_SESSION['login'];
    //variables qui permet de corriger les liens dans le header
    $jeux = 1;
    //random entre les mots du dico.txt
    $desired_line = rand(0,369086);
    $i=0;

    //ouvre le dico et choisit un mot
$handle = fopen("../../Ressource/Dictionnaire/dico.txt", "r");
if ($handle) {
    while ((($line = fgets($handle)) !== false) && $desired_line!=$i) {
        $i++;
    }
    $mot = $line;



    fclose($handle);
} else {
    echo 'error reading file';

}

?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8" />
    <title>Pendu</title>
    <!-- <link rel="stylesheet" href="../Styles/StyleJeux.css" /> ->
    <!-- Boostrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../../JavaScript/pendu.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>

<header>
    <?php require "../header.php" ?>
</header>

<body>
<section class="boite_jeu">
    <h1>Pendu</h1>
    <article>

    </article>
</section>

    <?php
    //split du mot
        $arr1 = str_split($mot);
        $size = count($arr1);
        unset($arr1[$size-1]);
        unset($arr1[$size-2]);

        echo "<br><br>";

    ?>

<img style="float: right" id="imgpendu" src="../../Ressource/Pendu/Pendu0.png" alt="pendustate">
<div style="text-align: center" id="afficheMot">





        <?php
            for ($i=0;$i<count($arr1);$i++){
                echo "<div style=\"display: inline; font-size: 75px\" id=\"lettre" . $i . "\"> _ </div>";
            }
            $size = count($arr1);
        ?>

</div>

<!-- Variables cachéées poour le JavaScript-->
<div style="visibility: hidden" id="recup"><?php echo json_encode($arr1); ?></div>
<div style="visibility: hidden" id="session_login"><?php echo $login ?></div>
<div style="visibility: hidden" id="tailletab"><?php echo $size; ?></div>


<br>
<br>

<form style="text-align:center;width: 50%;margin: auto" method="post" action="">


    <div class="form-group">
        <label  for="LettrePendu">Votre lettre</label>
        <input
                style= "text-transform:uppercase; width:10%; font-size: xx-large; text-align: center; margin: auto"
                maxlength="1"
                type="text" class="form-control" name="lettrePendu" id="lettrePendu"
        >

    </div>

    <div style="margin-top:10px" class="form-group">
        <!-- Button -->
        <div  class="col-sm-12 controls">
            <input id="lettrebutton"  type="submit" value="Valider" class="btn btn-success"> </input>

        </div>
    </div>

</form>



</body>

<footer>
</footer>

</html>
