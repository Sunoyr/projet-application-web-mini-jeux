<?php
    include "../requetes.php";
    error_reporting(0);
    $jeux = 1;
    if (!isset($_SESSION['login'])){
        header('Location:../connexion.php?location=' . urlencode($_SERVER['REQUEST_URI']));
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mots-Mếlés</title>
    <!-- <link rel="stylesheet" href="../Styles/StyleJeux.css" /> -->

    <!-- Boostrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="../../JavaScript/motsMelee.js"></script>
    <link rel="stylesheet" href="../../Styles/styleMotsMelee.css">
</head>

<header>
    <?php require "../header.php" ?>
</header>

<body>
    <section class="boite_jeu">
        <h1>Mots-Mêlés</h1>
        <article>

        </article>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-8">
                <?php
                //----------------------------------------------------------------------
    //  AUTHOR	: Jean-Francois GAZET
    //  WEB		: http://www.jeffprod.com
    //  TWITTER	: @JeffProd
    //  MAIL	: jeffgazet@gmail.com
    //  LICENCE	: GNU GENERAL PUBLIC LICENSE Version 2, June 1991
    //----------------------------------------------------------------------

                require_once '../class.grid.php';
                require_once '../class.word.php';

                $grid=new Grid();
                $grid->gen();
                echo $grid->render();
                ?>
            </div>
            <div class="col" style="overflow:scroll; border:#000000 1px solid;height: 500px">
                <?php
                echo "Les mots à trouver sont (".$grid->getNbWords().") :<br>";
                echo $grid->getWordsList("<br>");
                ?>
            </div>
        </div>
    </div>

    <section class="boite_infos">
        <h1>Infos Partie</h1>
    </section>
</body>

<footer>
</footer>
</html>
