<?php
    include "../requetes.php";
    error_reporting(0);
    if (!isset($_SESSION['login'])){
        header('Location:../connexion.php?location=' . urlencode($_SERVER['REQUEST_URI']));
    }
    $jeux = 1;
    $login=$_SESSION['login'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Sudoku classique</title>
    <!-- <link rel="stylesheet" href="../Styles/StyleJeux.css" /> -->

    <!-- Boostrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../Styles/StyleJeux.css">
    <link rel="stylesheet" href="../../Styles/sudoku.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../JavaScript/sudoku.js-master/sudoku.js" ></script>

</head>

<header>
    <?php require "../header.php" ?>
</header>

<body>
<section class="boite_jeu">
    <h1>Sudoku</h1>
    <article id="frame">

        <table>
            <tr>
                <td id="11"></td>
                <td id="12"></td>
                <td id="13" ></td>

                <td class="marge" rowspan="12"></td>

                <td id="14" ></td>
                <td id="15" ></td>
                <td id="16"></td>

                <td class="marge" rowspan="12"></td>

                <td id="17" ></td>
                <td id="18" ></td>
                <td id="19" ></td>
            </tr>

            <tr>
                <td id="21" ></td>
                <td id="22" ></td>
                <td id="23" ></td>
                <td id="24" ></td>
                <td id="25" ></td>
                <td id="26" ></td>
                <td id="27" ></td>
                <td id="28" ></td>
                <td id="29" ></td>
            </tr>

            <tr>
                <td id="31" ></td>
                <td id="32" ></td>
                <td id="33" ></td>
                <td id="34" ></td>
                <td id="35" ></td>
                <td id="36" ></td>
                <td id="37" ></td>
                <td id="38" ></td>
                <td id="39" ></td>
            </tr>

            <tr><td colspan="12" class="marge"></td></tr>

            <tr>
                <td id="41" ></td>
                <td id="42" ></td>
                <td id="43" ></td>
                <td id="44" ></td>
                <td id="45" ></td>
                <td id="46" ></td>
                <td id="47" ></td>
                <td id="48" ></td>
                <td id="49" ></td>
            </tr>

            <tr>
                <td id="51" ></td>
                <td id="52" ></td>
                <td id="53" ></td>
                <td id="54" ></td>
                <td id="55" ></td>
                <td id="56" ></td>
                <td id="57" ></td>
                <td id="58" ></td>
                <td id="59" ></td>
            </tr>

            <tr>
                <td id="61" ></td>
                <td id="62" ></td>
                <td id="63" ></td>
                <td id="64" ></td>
                <td id="65" ></td>
                <td id="66" ></td>
                <td id="67" ></td>
                <td id="68" ></td>
                <td id="69" ></td>
            </tr>

            <tr><td colspan="12" class="marge"></td></tr>

            <tr>
                <td id ="71" ></td>
                <td id ="72" ></td>
                <td id ="73" ></td>
                <td id ="74" ></td>
                <td id ="75" ></td>
                <td id ="76" ></td>
                <td id ="77" ></td>
                <td id ="78" ></td>
                <td id ="79" ></td>
            </tr>

            <tr>
                <td id="81" ></td>
                <td id="82" ></td>
                <td id="83" ></td>
                <td id="84" ></td>
                <td id="85" ></td>
                <td id="86" ></td>
                <td id="87" ></td>
                <td id="88" ></td>
                <td id="89" ></td>
            </tr>

            <tr>
                <td id="91"></td>
                <td id="92"></td>
                <td id="93"></td>
                <td id="94"></td>
                <td id="95"></td>
                <td id="96"></td>
                <td id="97"></td>
                <td id="98"></td>
                <td id="99"></td>
            </tr>
        </table>
        <button type="button" onclick="annuleCoup();affichage(diff)" >Annuler</button>
    </article>
</section>
<section class="boite_infos">
    <h1>Informations de la partie</h1>
    <article id="infoPartie">

        <form id="formulaire">
            <p>
                Veuillez choisir la difficulté :<br />
                <input type="radio" name="difficulte" value="easy" id="easy"/>                          <label>Facile</label><br />
                <input type="radio" name="difficulte" value="medium" id="medium" checked="checked"/>    <label>Moyen</label><br />
                <input type="radio" name="difficulte" value="hard" id="hard" />                         <label>Difficile</label><br />
                <input type="radio" name="difficulte" value="very-hard" id="very-hard" />               <label>Très Difficile</label><br />
                <input type="radio" name="difficulte" value="insane" id="insane" />                     <label>Guerrier</label><br />
                <input type="radio" name="difficulte" value="inhuman" id="inhuman" />                   <label>Suicidaire</label><br />
            </p>
            <input type="button" onclick="lancepartie();" value="Valider">
        </form>
        <div style="visibility: hidden" id="log"><?php echo $login ?></div>
    </article>
    <script>

        /**Fonction qui fait renplace le formulaire après ca validation par les informations de partie
         * @param  diff chaine de caractère qui contient la difficulté du sudoku */
        function validation(diff){
            let info=document.getElementById("infoPartie");
            let str=" ";
            for (let i=0;i<symboles2.length;i++){
                str+=symboles2[i];
                if(i<symboles2.length-1){
                    str+="-";
                }
            }
            info.innerHTML="<p>Niveau :"+diff+"<br />Symboles :"+str+"</p><br /><p>Score : "+score+" </p>";
        }

        /**Fonction qui s'active a la validation du formulaire
         * elle genere la grille du sudoku grace la fonction sudoku.generate() de  l'API (sudoku.js-master)
         * puis fait les appel nécéssaire aux lancement d'une partie*/
        function lancepartie() {
            let diff= $('input[name=difficulte]:checked').val();
            let sd = sudoku.generate(diff, true);
            initierGrille(sd, true, true);
            initierDifficulteScore(diff);
            affichage();
            setOnclicks();
            validation(diff);
            difficult=diff;
        }
    </script>
</section>
<p hidden="hidden" id="log"> <?php echo $login?> </p>
<script src="../../JavaScript/Sudoku.js"></script>

</body>

<footer>
</footer>
</html>
