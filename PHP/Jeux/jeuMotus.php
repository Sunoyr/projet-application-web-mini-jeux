<?php
    include "../requetes.php";
    error_reporting(0);
    $jeux = 1;
    if (!isset($_SESSION['login'])){
        header('Location:../connexion.php?location=' . urlencode($_SERVER['REQUEST_URI']));
    }


    $login = $_SESSION['login'];
    $jeux = 1;
    $desired_line = rand(0,369086);
    $i=0;

    //ouvres le dico et choisis un mot au hasard
    $handle = fopen("../../Ressource/Dictionnaire/dico.txt", "r");
    if ($handle) {
        while ((($line = fgets($handle)) !== false) && $desired_line!=$i) {
        $i++;
    }
    $mot = $line;



    fclose($handle);
    }
    else {
        echo 'error reading file';
    }

    //split du mot
        $arr1 = str_split($mot);
        $size = count($arr1);

        unset($arr1[$size-1]);
        unset($arr1[$size-2]);

        $size = count($arr1);



?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8" />
    <title>Motus</title>
    <!-- <link rel="stylesheet" href="../Styles/StyleJeux.css" /> ->
    <!-- Boostrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>

<header>
    <?php require "../header.php" ?>
</header>

<body>
<section class="boite_jeu">
    <h1>Motus</h1>

</section>



<section class="boite_infos">

   <table style="margin: auto" id="motus" border="1">


       <?php for ($i=1;$i<9;$i++){ ?>
             <tr class="table-primary">
                 <?php for ($j=0;$j<$size;$j++){?>
                     <td id=<?php echo'"'.$i.$j.'"' ?> style="<?php if($j==0){echo "color:white;background-color:red;";}?>text-align:center;font-size: 24px;" width="50" height="50">
                     <?php if($j == 0){
                         echo $arr1[0];
                     }
                     else{
                         echo '-';
                     }
                     ?>
                     </td>
                <?php }?>
             </tr>
       <?php }
       ?>

   </table>
       <br><br><br><br><br>

       <form style="text-align:center;width: 50%;margin: auto" method="post" action="">


           <div class="form-group">
               <label  for="LettrePendu">Votre mot</label>
               <input
                       style= "text-transform:uppercase; width:50%; font-size: xx-large; text-align: center; margin: auto"
                       maxlength=<?php echo "'".$size."'" ?>
                       type="text" class="form-control" name="motMotus" id="motMotus"
               >

           </div>

           <div style="margin-top:10px" class="form-group">
               <!-- Button -->
               <div  class="col-sm-12 controls">
                   <input id="motbutton"  type="submit" value="Valider" class="btn btn-success"> </input>

               </div>
           </div>

       </form>

<!-- Variables cachées pour le JavaScript -->
    <div style="visibility: hidden" id="recup"><?php echo json_encode($arr1); ?></div>
    <div style="visibility: hidden" id="session_login"><?php echo $login ?></div>
    <div style="visibility: hidden" id="tailletab"><?php echo $size; ?></div>





</section>
<script src="../../JavaScript/motus.js"></script>
</body>

<!--
<footer>

</footer>
-->
</html>


