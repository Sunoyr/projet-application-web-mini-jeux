
<?php session_start() ?>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand -->
    <?php

        if (isset($jeux)){
            $link = "../../Styles/Image/logo.png";
            $link2 = '../';
            $link3 = '../../';
        }
        elseif (isset($index)){
            $link = "Styles/Image/logo.png";
            $link2 = 'PHP/';
            $link3 = '';

        }
        else{
            $link2='';
            $link = "../Styles/Image/logo.png";
            $link3 = '../';


        }
    ?>

    <a class="navbar-brand" href="<?php echo $link3 ?>index.php"> <img src=<?php echo $link; ?>></a>

    <!-- Links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<?php echo $link2 ?>leaderboard.php">Leaderboard</a>
        </li>

        <!-- Dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                Liste des jeux
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?php echo $link2 ?>Jeux/jeuMotus.php">Motus</a>
                <a class="dropdown-item" href="<?php echo $link2 ?>Jeux/jeuMotsMeles.php">Mots mélés</a>
                <a class="dropdown-item" href="<?php echo $link2 ?>Jeux/jeuPendu.php">Pendu</a>
                <a class="dropdown-item" href="<?php echo $link2 ?>Jeux/jeuSudoku.php">Sudoku</a>
                <a class="dropdown-item" href="<?php echo $link2 ?>Jeux/jeuSudokuLettre.php">Sudoku Lettre</a>
            </div>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <?php
            if (!isset($_SESSION['login'])) {
                ?>
                <li><a href="<?php echo $link2 ?>inscription.php">
                        <button type="button" class="btn btn-light">Inscription</button>
                    </a></li>
                <li><a href="<?php echo $link2 ?>connexion.php">
                        <button type="button" class="btn btn-success">Connexion</button>
                    </a></li>
                <?php
            } else {
                echo "<li style='margin-right: 20px;'><a style='color:white' href='#'>" . $_SESSION['login'] . "</a></li>";
                ?>
                <li><a href="<?php echo $link2 ?>deconnexion.php">
                        <button type="button" class="btn btn-success">Deconnexion</button>
                    </a></li>
           <?php }
        ?>

    </ul>
</nav>
<br>