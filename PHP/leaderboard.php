<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Leaderboard</title>

    <meta charset="utf-8">
    <link rel="stylesheet" href="../Styles/styleHeaderFooter.css"/>

    <!-- Boostrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

</head>

<header>
    <?php require "header.php" ?>
</header>
<body>
    <select name="ordre" id="ordre">
        <option value="general">Général</option>
        <option value="sudoku">Sudoku</option>
        <option value="sudokulettres">Sudoku Letrres</option>
        <option value="motus">Motus</option>
        <option value="pendu">Pendu</option>
        <option value="motsmeles">Mots mélés</option>

    </select>

    <table class="table" id="leaderboard">
        <thead>
        <tr>
            <th scope="col">Rang</th>
            <th scope="col">Pseudo</th>
            <th scope="col">Score</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
        </tr>
       </tbody>

    </table>
    <script>
        $(document).ready(function(){

            $("#leaderboard").find("tr:gt(0)").remove();
            var order = $("#ordre").val();

            jQuery.ajax({
                type: "POST",
                url: 'requetes.php',
                dataType: 'text',
                data: {functionname: 'leader', arguments: [order]},

                success: function (obj, textstatus) {
                    console.log(obj);
                    $("#leaderboard").append(obj);
                }
            });

            $("#ordre").change(function (e) {
                $("#leaderboard").find("tr:gt(0)").remove();
                var order = $("#ordre").val();

                jQuery.ajax({
                    type: "POST",
                    url: 'requetes.php',
                    dataType: 'text',
                    data: {functionname: 'leader', arguments: [order]},

                    success: function (obj, textstatus) {
                        console.log(obj);
                        $("#leaderboard").append(obj);
                    }
                });
            });
        });
    </script>
</body>
<footer>
</footer>
</html>
