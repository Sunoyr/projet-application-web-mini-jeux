<?php
require 'tools.php';
require_once 'requetes.php';

// Ma clé privée
$secret = "6LfpvXQUAAAAALkYJj81HpZ5mdZmc5Xn9VVR83AK";
/* IMPORTANT



*/
// Paramètre renvoyé par le recaptcha
$response = $_POST['g-recaptcha-response'];
// On récupère l'IP de l'utilisateur
$remoteip = $_SERVER['REMOTE_ADDR'];



$url = 'https://www.google.com/recaptcha/api/siteverify';
$data = array(
    'secret' => $secret,
    'response' => $_POST["g-recaptcha-response"]
);

$query = http_build_query($data);

$options = array(
    'http' => array (
        'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
            "Content-Length: ".strlen($query)."\r\n".
            "User-Agent:MyAgent/1.0\r\n",
        'method' => 'POST',
        'content' => $query
    )
);
$context  = stream_context_create($options);
$verify = file_get_contents($url, false, $context);
$captcha_success=json_decode($verify);

if ($captcha_success->success==true) {


    $pseudo = htmlspecialchars($_POST['pseudo']);
    $pass = htmlspecialchars($_POST['password']);
    $email = htmlspecialchars($_POST['email']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $nom = htmlspecialchars($_POST['nom']);
    $sexe = htmlspecialchars($_POST['sexe']);



    inscription($bdd, $nom, $prenom, $pseudo, $email, $sexe, $pass);


    header('Location:connexion.php');
}

else {

// C'est un robot ou le code de vérification est incorrect

    header( "refresh:5; url=index.php" );
    echo 'Le captcah n\'a pas été validé, vous allez être redirigé automatiquement...';

}

?>