<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <title>Inscription</title>
    <meta name="author" content="">
    <meta name="description" content="">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="../JavaScript/inscription.js"></script>

</head>

<header><?php include 'header.php' ?></header>

<body>

<form style="width: 50%;margin: auto" method="post" action="traitementInscription.php">



    <div class="form-group">
        <label for="exampleInputEmail1">Adresse E-mail</label>
        <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">

    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">Pseudo </label>
        <input type="text" class="form-control" name="pseudo" id="pseudo">
    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">Nom </label>
        <input type="text" class="form-control" name="nom" id="nom">
    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">Prénom </label>
        <input type="text" class="form-control" name="prenom" id="prenom">
    </div>
    <br>

    <div class="form-group">
        Sexe
    </div>
    <div class="form-check form-check-inline">
        <label class="form-check-label">
            <input class="form-check-input" type="radio" name="sexe" id="inlineRadio1" value="M"> Masculin
        </label>
    </div>
    <div class="form-check form-check-inline">
        <label class="form-check-label">
            <input class="form-check-input" type="radio" name="sexe" id="inlineRadio2" value="F"> Féminin
        </label>
    </div>



    <br><br><br>
    <div class="form-group">
        <label for="exampleInputPassword1">Mot de passe</label>
        <input required style="" type="password" name="password" class="form-control" id="password" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$">
        <small  id="passHelper" class="form-text text-muted">Doit contenir au moins 8 caractères, une majuscule et un chiffre</small>
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Confirmez votre mot de passe</label>
        <input required type="password" class="form-control" id="confirm_password" >
    </div>

    <span id='message'></span>


    <br><br>

    <div class="form-check">
        <input class="form-check-input" id="cnil" type="checkbox" value="" >
        <label class="form-check-label" for="defaultCheck1">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consequat.
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" id="age" type="checkbox" value="" >
        <label class="form-check-label" for="defaultCheck2">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consequat.
        </label>
    </div>
    <span id="message2"></span>
    <br>
    <div class="g-recaptcha" data-sitekey="6LfpvXQUAAAAAJYnjh2G2tocHmqualW221zJJaAP"></div>

    <br><br>



    <button style="display: none" type="submit" id="submit" class="btn btn-primary" >Valider</button>

</form>


</body>

</html>


