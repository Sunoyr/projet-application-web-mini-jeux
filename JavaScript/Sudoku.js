
/**Definition d'une matrice 9x9 qui contiendra la grille de sudoku*/
const taille=9;
let grille=Array();
for(let i=0;i< taille;i++){
    grille.push(new Array(taille))
}

/**Le tableau symbole contiendra les symbole de la grille (affiché a l'écran la grille contiendra des chiffre de 1 a 9)*/
let symboles2 = "00000000";


/** Pile qui sauvegarde les coups fait précedemment (pour une eventuelle annulation d'un ou plusieurs coup) */
let pileCoupsJoues = Array();

/** Matrice 3x3 qui permet la verifiaction dans une des neuf case 3x3 du sudoku */
let carre = Array();
for(let i=0;i<3;i++){
    carre.push(new Array(3))
}

/**Score et si le joueur fait un sans-faute (booleen) on attribura un bonus de point */
let score=0;
let sansFaute=true;

/**Variables qui contiendront en chaine de caractère la difficulté et le type de jeu (chiffre ou lettre) */
let difficult="";
let jeu="";

/**Variable qui sert a recupérer le pseudo de l'utilisateur*/
let login;




/**Fonction qui ajoute des point au score total en fonction de la difficulté
 * @param diff contient une chaine de caractères du niveau de difficulté */
function initierDifficulteScore(diff){
    if(diff === "inhuman"){
        score+=50
    }else if(diff === "insane"){
        score+=40;
    }else if(diff === "very-hard"){
        score+=30;
    }else if(diff === "hard"){
        score+=20;
    }else if(diff === "medium"){
        score+=10;
    }// si la difficulté est facile on ne donne pas de point pour la difficulté
}

/**Le tableau symbole contiendra les symbole de la grille (affiché a l'écran la grille contiendra des chiffre de 1 a 9)
 * @param chiffre booleen
 * @param rand booleen*/
function initierSymbole(chiffre,rand) {
    if (chiffre) {
        jeu = "sudoku";
        symboles2 = "123456789";
    } else {
        jeu="sudokulettres";
        if (rand === 'true') {
            let r;
            score += 50;
            symboles2 = "";
            for (let i = 0; i < taille; i++) {
                r = randomchar();
                do {
                    r = randomchar();
                } while (symboles2.includes(r));
                symboles2 = symboles2 + r + "";
            }
        } else {
            symboles2 = "ABCDEFGHI";
            score += 25;
        }
    }
}

/**Fonction qui retourne la selection aléatoire de caractère*/
function randomchar() {

    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789";
    return possible.charAt(Math.floor(Math.random() * possible.length));
}



/** fonction qui initialise la grille la pile et les symbole
 *  @param grilledepart une chaine de caractere qui contient la grille de depart (genérer dans la page php du sodoku )
 *  @param chiffre un booleen qui signifie le type de sudoku chiffre ou lettre
 *  @param rand est un booleen qui permet si le sudoku lettre est fait avec des symbole aléatoire ou non */
function initierGrille(grilledepart,chiffre,rand) {
    let z=-1;
    for (let i = 0; i < grille.length; i++) {
        for (let j = 0; j < grille.length; j++) {
            z = grilledepart.charAt(i*9 + j);
            if (z === '.') {
                grille[i][j] = 0;
            } else{
                grille[i][j] = parseInt(z);
            }
        }
    }
    initialisationPile();
    initierSymbole(chiffre,rand);
}

/**Fonction qui initialise la pile avec la grille de depart (note: fonction appelé une seule fois après l'initialisation de la grille de sudoku)*/
function initialisationPile() {
    pileCoupsJoues.push();
    pileCoupsJoues[0] = new Array(taille);
    for (let x=0;x<grille.length;x++) {
        pileCoupsJoues[0][x] = new Array(taille);
    }

    for (let x=0;x<grille.length;x++){
        for (let j=0;j<grille[x].length;j++){

            pileCoupsJoues[0][x][j]=grille[x][j];
        }
    }

}

/**Fonction qui update la pile en rajoutant la nouvelle grille apres un coup joué */
function copyGrille(){
    pileCoupsJoues.push(Array());
    pileCoupsJoues[pileCoupsJoues.length-1] = new Array(taille);
    for (let x=0;x<grille.length;x++) {
        pileCoupsJoues[pileCoupsJoues.length - 1][x] = new Array(taille);
    }

    for (let x=0;x<grille.length;x++){
        for (let j=0;j<grille[x].length;j++){
            pileCoupsJoues[pileCoupsJoues.length-1][x][j]=grille[x][j];
        }
    }
}

/**Fonction de verification de la validité d'un coup pour la ligne
 * @param x la ligne de la grille que laquelle on joue
 * @param val la valeur joué
 * */
function verifierLigne(x, val){ //retourne true si le coups peut être joue par rapport a la ligne
    for(let i=0;i<grille.length;i++){
        if(grille[x][i]===val){
            alert("ce nombre est déja present sur la ligne");
            return false;
        }
    }
    return true;
}

/**Fonction de verification de la validité d'un coup pour la colonne
 * @param y la colonne de la grille que laquelle on joue
 * @param val la valeur joué
 * */
function verifierColonne(y,val){ //retourne true si le coups peut être joue par rapport a la colonne
    for(let i=0;i<grille.length;i++){
        if(grille[i][y]===val){
            alert("ce nombre est déja present sur la colonne");
            return false;
        }
    }
    return true;
}

/** Retourne le numéro de carré suivant l'ordre de lecture  de gauche a droite et de haut en bas.(1 est la première case 9 la dernière)
 * @param x ligne ou le coup est joué
 * @param y colonne ou le coup est joué
 * */
function trouvercellule3x3(x,y){
    let res;
    if( x<9 && x>=0 && y<9 && y>=0){


        let col = (x===0)?  1 :parseInt(x/3)+1;
        let lign = (y===0)?  1 :parseInt(y/3)+1;



        if(lign===1 && col === 1)res= 1;
        else if(lign === 1 && col === 2)  res= 2;
        else if(lign === 1 && col === 3)  res= 3;
        else if(lign === 2 && col === 1)  res= 4;
        else if(lign === 2 && col === 2)  res= 5;
        else if(lign === 2 && col === 3)  res= 6;
        else if(lign === 3 && col === 1)  res= 7;
        else if(lign === 3 && col === 2)  res= 8;
        else if(lign === 3 && col === 3)  res= 9;
    }else{
        alert ("erreur");
        res=-1;
    }
    return res;
}

/** Retourne le carré 3x3 correspondant au numero trouver par trouverCellule3x3
 * @param x numero du carré a verifier
 * */
function getCarre3x3(x){
    //reinitialisation du tableau (-1 pour que toutes errreur soit évidente)
    for (let i=0;i<3;i++){
        for (let j=0;j<3;j++){
            carre[i][j] = -1;
        }
    }

    let xmin,ymin;
    switch(x){
        case 1:
            xmin=0;
            ymin=0;
            break;

        case 2:
            xmin=0;
            ymin=3;
            break;

        case 3:
            xmin=0;
            ymin=6;
            break;
        case 4:
            xmin=3;
            ymin=0;
            break;

        case 5:
            xmin=3;
            ymin=3;
            break;

        case 6:
            xmin=3;
            ymin=6;
            break;

        case 7:
            xmin=6;
            ymin=0;
            break;

        case 8:
            xmin=6;
            ymin=3;
            break;

        case 9:
            xmin=6;
            ymin=6;
            break;

        default:
            alert("erreur case 3x3");
    }
    for (let i=0;i<carre.length;i++){
        for (let j=0;j<carre[i].length;j++){
            carre[i][j] = grille[i+ymin][j+xmin];
        }
    }
}

/**Fonction qui grace a getCarre et trouvercellule3x3 verrifie la validité d'un coup dans la zone cocerné (le carré 3x3)
 * @param x ligne du coup joué
 * @param y colonne du coup joué
 * @param val valeur joué (entre 1 et 9)*/
function verifierCarre(x,y,val){
    getCarre3x3(trouvercellule3x3(x,y));
    for (let i=0;i < carre.length;i++){
        for (let j=0;j < carre[i].length;j++){
            if(carre[i][j]===val){
                alert("ce nombre est déja present dans le carré de 3x3");
                return false;
            }
        }
    }
    return true;
}


/**Fonction qui va appeler les différente verification ligne colonne et carre pour verifier la valitdité d'un coup ainsi que
 * verifier que la valeur val est bien un symbole de la partie actuel(que val appartient a symboles2),
 * la foncton verifie egalement que la case est bien vide (grille[x][y] === 0) puis fait le coup puis met jour la pile,
 * puis verifie si la partie n'est pas terminé si le coup n'est pas valide on enlève de points(moins que les point soit nuls)
 * si la partie es fini on envoie les score dans la base de donnée
 * @param X ligne du coup joué
 * @param Y colonne du coup joué
 * @param val symbole joué */
function jouerCoup(X,Y,val) {

    //verification que val est un symbole de la partie est affectation de sont equivalent en Chiffre
    let chiffre=0;
    let z=0;

    while (z<9 && chiffre === 0) {
        if (symboles2[z] === val) {
            chiffre = z+1;
        }
        z++;
    }
    if (chiffre !== 0 && grille[X][Y]===0 && X >= 0 && X < 9 && Y >= 0 && Y < 9 && verifierLigne(X, chiffre) && verifierColonne(Y, chiffre) && verifierCarre(X, Y, chiffre)){
        grille[X][Y] = chiffre;
        copyGrille();
        if(grilleResolue()){
            if (sansFaute){
                score+=50;
            }
            jQuery.ajax({
                type: "POST",
                url: '../requetes.php',
                dataType: 'text',
                data: {functionname: 'add', arguments: [jeu, score,login, 0]},

                success: function (obj, textstatus) {
                    if( !('error' in obj) ) {
                        $success = obj.result;
                    }
                    else {
                        console.log(obj.error);
                    }
                }
            });
        }
    }else if(val != null){
        if(score>=10){
            score-=10;
        }else{
            score=0;
        }
        sansFaute=false;
        //alert("coup impossible :(");
    }

}

/**Fonction qui annule un coup et revient a la grille pecedente  puis supprime la derniere grille de la pile,
 * la fonction fait tout de même la verification qu'un ou plusieurs coups on déjà été jouées
 * puis enfin ont reaffiche la grille*/
function annuleCoup(){
    if(pileCoupsJoues.length < 2){
        alert("impossible d'anuler un coup sommes a la grille de base");//TODO reformuler la phrase
    } else {

        //on enlève des point et supprime les point bonus de sans-fautes
        if(score>=10){
            score-=10;
        }
        sansFaute=false;

        //on supprime le coups et la grille revient a l'état precédent
        pileCoupsJoues.pop();
        for(let i=0;i<grille.length;i++){
            for (let j=0;j<grille[i].length;j++){
                grille[i][j] = pileCoupsJoues[pileCoupsJoues.length - 1][i][j];
            }
        }
        //on réaffiche la grille
        affichage();
    }
}

/**Fonciton qui verifie si toute la grille a été complété et retourne un booléen*/
function grilleResolue(){//retourn un booleen (vrai si la grille est fini)
    for(let i=0;i<taille;i++){
        for(let j=0;j<taille;j++){
            if(grille[i][j] === 0){
                return false
            }
        }
    }
    return true;
}

/**Fonction qui affiche la grille dans le tableau de la page .php  la case est considéré vide ("") si elle a la valeur 0 et
 * on affiche la valeur du symbole2 de la [valeur-1] car 1 est le premier caractère de symbole2
 * affiche egalement le score la difficulté et les symbole utilisé pour la partie dans le menu a droite*/
function affichage(){
    let str;
    for (let i=1;i<=taille;i++){
        for (let j=1;j<=taille;j++) {
            str = ("" + i + "" + j + "");
            if (grille[i - 1][j - 1] !== 0) {
                let val=grille[i - 1][j - 1];
                document.getElementById(str).innerHTML =(symboles2.charAt(val-1));
            }else{
                document.getElementById(str).innerHTML ="";
            }
        }
    }
    let str2=" ";
    for (let i=0;i<symboles2.length;i++){
        str2+=symboles2[i];
        if(i<symboles2.length-1){
            str2+="-";
        }
    }
    document.getElementById("infoPartie").innerHTML="<p>Niveau : " + difficult + "<br /> Symboles :"+str2+"</p><br /><p>Score : "+score+" </p>" ;
}


/**fonction qui permet de demander la valeur a l'utilisateur d'entrez la valeur qu'il veut jouer dans la case où il a cliqué*/
function clicktoPlay(doc){
    let x= doc.getAttribute('id').charAt(0);
    let y= doc.getAttribute('id').charAt(1);
    if(grille[x-1][y-1] ===0){
        let val=prompt("quelle valeur souhaitez vous entrez ?");
        jouerCoup(x-1,y-1,val);
        affichage();

    }

}

/**fonction qui met en place les onClick sur les case du tableau sur la page .php du sudoku*/
function setOnclicks() {
    for (let i=1;i<=taille;i++){
        for (let j=1;j<=taille;j++) {
            let doc=document.getElementById("" + i + "" + j + "");
            doc.onclick=function(){clicktoPlay(doc);};
        }
    }
}

