$(document).ready(function(){

    //fonction retournant les indexs des occurences d'une valeur dans un array
    function getAllIndexes(arr, val) {
        var indexes = [], i = -1;
        while ((i = arr.indexOf(val, i+1)) != -1){
            indexes.push(i);
        }
        return indexes;
    }

    var jeu = 'motus';
    var points = 0; //compteur de points
    var login = $("#session_login").text();
    var ligne  = 0;
    var taille = $("#tailletab").text();
     //array servant à vérifiant quelles lettres sont déjà validées dans le mot caché
    var ok = 0;
    var dico = 0; //vaut 0 ou 1 selon si le mot est présent dans le dico (modifié par AJAX)







    console.log(login);
    $('#motbutton').click(function (e) {
        dico = 0;
        var utilises = Array(taille);
        utilises.fill('false');
        ligne++;
        var compteLettres = 0;
        e.preventDefault();
        var arraylettres = $("#recup").text();
        var motJS = $.parseJSON(arraylettres);
        var lettreutil2 = $("#motMotus").val();
        var lettreutil = lettreutil2.toUpperCase();
        var words = lettreutil.split('');
        console.log(motJS);


        //cherche si l emot entré est dans le dico
        jQuery.ajax({
            type: "POST",
            url: '../requetes.php',
            dataType: 'text',
            data: {functionname: 'verifDico', arguments: lettreutil},

            success: function (obj, textstatus) {
                console.log(obj);

                if (obj == 1) {
                    dico = 1;
                }
            }
        });

        var id="l";
        console.log(utilises);
        console.log(dico);

        setTimeout( function()
        {
            console.log(dico);
            if ( dico==1) {

                //On cherche les rouges
                for (let i = 0; i < taille; i++) {

                    id = "" + ligne + i;
                    $('#' + id).text(words[i]);


                    if (words[i] == motJS[i]) {
                        $('#' + id).text(words[i]);
                        $('#' + id).css("background-color", "red");
                        $('#' + id).css("color", "white");

                        utilises[i] = 'true';

                        points += 5;
                        compteLettres++;
                    }
                }
                console.log(utilises);


                //on cherche les jaunes
                for (let i = 0; i<taille;i++) {
                    ok=0;
                    id = "" + ligne + i;
                    $('#' + id).text(words[i]);
                    if ((jQuery.inArray(words[i], motJS) != -1)) {


                        var occurences = getAllIndexes(motJS, words[i]);
                        var long = occurences.length;
                        for (let j = 0; j < long; j++) {
                            if ((utilises[occurences[j]] != 'true') && ok==0) {
                                console.log(words[i]);
                                console.log(occurences);
                                console.log(j);
                                utilises[occurences[j]] = 'true';
                                console.log(id);
                                compteLettres++;
                                $('#' + id).css("background-color", "yellow");
                                points += 2;
                                ok = 1;
                            }
                        }
                    }
                }






                //si gagné
                if (compteLettres == taille) {
                    if (erreursCount == 0) {
                        points += 10;
                    }
                    if (points < 0){
                        points = 0;
                    }
                    alert("Gagné ! Vous avez gagné " + points + " points !");
                    //Entrée du score en BDD
                    jQuery.ajax({
                        type: "POST",
                        url: '../requetes.php',
                        dataType: 'text',
                        data: {functionname: 'add', arguments: [jeu, points, login, 1]},

                        success: function (obj, textstatus) {
                            if( !('error' in obj) ) {
                                $success = obj.result;
                            }
                            else {
                                console.log(obj.error);
                            }
                        }
                    });
                }




                //Si le max d'erreurs est atteint on stop le jeu

                else    if (ligne==8){
                    if (points < 0){
                        points = 0;
                    }
                    console.log(login);
                    //Entrée du score en BDD
                    alert("Perdu ! Le mot était : "+ motJS + " . Vous avez gagné " + points + " points !\n Retentez votre chance");
                    jQuery.ajax({
                        type: "POST",
                        url: '../requetes.php',
                        dataType: 'text',
                        data: {functionname: 'add', arguments: [jeu, points,login, 0]},

                        success: function (obj, textstatus) {
                            if( !('error' in obj) ) {
                                $success = obj.result;
                            }
                            else {
                                console.log(obj.error);
                            }
                        }
                    });
                }



            }
            else{
                alert("Mot invalide");


            }
        }, 2000); // timer car le fichier est grand et la recherche nécessite qques secondes


        $("#motMotus").val(''); //reset du champ mot

    });

});