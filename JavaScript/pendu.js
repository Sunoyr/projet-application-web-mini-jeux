$(document).ready(function(){
    var jeu = 'pendu';
    var trouve = 0; //compteurs de lettres trouvées sert à arrêter le jeu
    var erreursCount = 0; //compteur d'erreurs global
    var erreur=1; //vaut 0 ou 1 sert à savoir si au moins une lettre a été trouvée après une proposiiton
    var points = 0; //compteur de points
    var perfect = 0; //vaut 0 ou 1 (savoir si lejoueur a trouvé le mot sans aucune erreur -> bonus points)
    var login = $("#session_login").text();

    console.log(login);
    $('#lettrebutton').click(function (e) {
        erreur = 1;
        e.preventDefault();
        var arraylettres = $("#recup").text();
        var motJS = $.parseJSON(arraylettres);
        var lettreutil2 = $("#lettrePendu").val();
        var lettreutil = lettreutil2.toUpperCase();


        var taille = $("#tailletab").text();

        var id="l";

        if (lettreutil!=""){

            //parcours du mot
            for (let i = 0 ; i<taille;i++){
                id = "lettre" + i;

                if (lettreutil == motJS[i]){
                    if ($('#'+id).text() != lettreutil){
                        $('#'+id).text(lettreutil);
                        trouve++;
                    }


                    erreur = 0;
                    points +=5;
                }


            }

            // Si toutes les lettres ont été trouvées

                if (trouve == taille) {
                    if (erreursCount == 0) {
                        points += 10;
                        perfect = 1;
                    }
                    if (points < 0){
                        points = 0;
                    }
                    alert("Gagné ! Vous avez gagné " + points + " points !");
                    jQuery.ajax({
                        type: "POST",
                        url: '../requetes.php',
                        dataType: 'text',
                        data: {functionname: 'add', arguments: [jeu, points, login, 1]},

                        success: function (obj, textstatus) {
                            if( !('error' in obj) ) {
                                $success = obj.result;
                            }
                            else {
                                console.log(obj.error);
                            }
                        }
                    });
                    location.reload();
                }


            //Si acune lettre ne correspond, on ajoute une erreur et on modife l'image
            if (erreur==1){
                erreursCount++;
                points -= 2;
                $("#imgpendu").attr('src','../../Ressource/Pendu/Pendu'+ erreursCount +'.png');
            }

            //Si le max d'erreurs est atteint on stop le jeu
            setTimeout(function(){
                if (erreursCount==11){
                    if (points < 0){
                        points = 0;
                    }
                    console.log(login);
                    alert("Perdu ! Vous avez gagné " + points + " points !\n Retentez votre chance");
                    jQuery.ajax({
                        type: "POST",
                        url: '../requetes.php',
                        dataType: 'text',
                        data: {functionname: 'add', arguments: [jeu, points,login, 0]},

                        success: function (obj, textstatus) {
                            if( !('error' in obj) ) {
                                $success = obj.result;
                            }
                            else {
                                console.log(obj.error);
                            }
                        }
                    });
                    location.reload();
                }
            }, 1000);


        }
        $("#lettrePendu").val(''); //reset du champ lettre

    });

});