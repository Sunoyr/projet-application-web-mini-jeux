$(function () {

    $('#password, #confirm_password').on('keyup', function () {

        if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('');
        }
        else {
            $('#message').html('Les mots de passe sont différents').css('color', 'red');
        }

    });
});

$(function () {

    $('#cnil,#age').on('change', function () {

        if ($('#cnil').is(':checked') && $('#age').is(':checked')) {
            $('#message2').html('');
        }
        else{
            $('#message2').html('Veuillez cocher toutes les cases pour continuer').css('color', 'red');
        }
    });
});

$(function () {

    $('#cnil,#age,#password,#confirm_password').on('keyup change', function () {
        if ($('#cnil').is(':checked') && $('#age').is(':checked') && $('#password').val()==$('#confirm_password').val() && $('#password').val()) {
            $('#submit').toggle(true);

        }
        else{

            $('#submit').toggle(false);
        }
    });
});