<?php $index = 1; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Accueil</title>

    <meta charset="utf-8">
    <link rel="stylesheet" href="Styles/styleHeaderFooter.css"/>

    <!-- Boostrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

</head>

<header>
    <?php require "PHP/header.php" ?>
</header>
<body>
    <div class="container">
        <h2>Accueil du site jeux</h2><br>
        <p>

            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ante orci, interdum lacinia tempus nec, pretium at orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent ex justo, convallis sed dolor non, elementum vulputate lorem. Duis tempor feugiat fermentum. Aenean hendrerit tempus augue vel dapibus. Ut viverra condimentum vehicula. Donec ut lectus id orci euismod ultricies id id lorem. Suspendisse at ante quis metus convallis malesuada sit amet sit amet turpis. Sed blandit rhoncus fermentum.

            Curabitur vel diam in nisl fermentum hendrerit. Proin sit amet nibh pharetra, lacinia risus eget, venenatis felis. Suspendisse potenti. Maecenas pretium mattis viverra. Integer tempor, neque eu sodales sodales, enim dolor commodo nulla, eget faucibus odio mi sit amet sem. Nullam ultricies id tellus eget scelerisque. Vestibulum ut nisi tincidunt, ultrices augue ut, posuere quam. Pellentesque venenatis elit sit amet lectus tincidunt, quis bibendum quam commodo. Vestibulum ultrices porta pulvinar. Quisque porttitor tristique egestas. Phasellus suscipit auctor ex, et consectetur odio fermentum non. Nam vitae ligula massa. Etiam sed lacus at magna volutpat accumsan. Duis dapibus, massa eget egestas lacinia, purus diam vehicula ipsum, vel suscipit dolor magna sit amet sapien. Praesent elit ante, euismod sed pretium in, consequat pulvinar metus.

            Pellentesque vitae condimentum quam. Nullam vulputate purus eget ante consequat rutrum. Sed vitae urna maximus, hendrerit eros non, semper nulla. Quisque ullamcorper justo lacus, in vulputate urna rutrum in. Nam a feugiat odio. Nam malesuada erat nunc, non volutpat enim pellentesque eget. Etiam leo magna, elementum ac nulla eu, commodo ultrices erat. Nam sem orci, convallis at pharetra at, consectetur nec libero. Aenean vitae tincidunt diam. Quisque quis sodales augue, non cursus orci. Proin est mi, placerat ac augue a, euismod bibendum sapien. Nunc aliquam nibh a lorem tincidunt, a gravida nisl cursus. Curabitur lobortis pellentesque ante, in pharetra urna tincidunt sed.

            Praesent eu est mauris. Ut ornare tincidunt egestas. Fusce at diam egestas, elementum augue sed, laoreet mi. Nam suscipit malesuada luctus. Sed tincidunt nibh nunc, a aliquam nisi interdum ut. Praesent ut fringilla nisi, non tristique metus. Donec vehicula, mauris ut efficitur dictum, tortor elit dignissim lacus, a interdum est diam at tellus. Proin vitae ornare nisl. Quisque imperdiet nunc id elit molestie mollis. Ut eu eros id odio venenatis tincidunt. Vestibulum semper augue at velit dignissim, vel tempus lacus faucibus. Suspendisse porta, tortor id ullamcorper lobortis, turpis risus malesuada nisi, nec ullamcorper mi nisi ornare erat. Fusce eget ipsum dictum, suscipit dolor in, vehicula nisi. Nullam ligula ante, malesuada at odio eu, facilisis consectetur orci. In non ipsum in lacus finibus vulputate.

            Donec consequat aliquet lorem, ut efficitur libero. Mauris ac purus vel nulla pretium pellentesque vel a metus. Quisque leo ipsum, tincidunt vitae mauris vitae, pulvinar interdum nisl. Nullam id luctus arcu. Vivamus ipsum velit, lobortis vitae fermentum nec, facilisis sed sem. In hac habitasse platea dictumst. Integer neque metus, ultricies dictum posuere ac, maximus nec quam. Quisque ac nibh vitae ipsum malesuada viverra. Etiam nec lorem vitae sem cursus luctus. Nullam et dui pretium, vulputate diam nec, malesuada elit. </p>
    </div>
</body>
<footer>
</footer>
</html>